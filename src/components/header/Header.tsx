import logo from "../../assets/game-logo.png";

function Header(): JSX.Element {
  return (
    <header>
      <img src={logo} className="App-logo" alt="logo" />
      <h1>Tic-Tac-Toe</h1>
    </header>
  );
}

export default Header;
