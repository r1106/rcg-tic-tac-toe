import { ITurn } from "../../types/interfaces/turn.interface";

function Logs({ turns }: { turns: ITurn[] }) {
  const logs = [...turns];
  const selectField: JSX.Element = <li>Please select a Field</li>;
  const logEntries: JSX.Element[] = logs.map((log, logIndex) => (
    <li key={logIndex}>
      {`Player ${log.playerSymbol} selected ${log.square.row} | ${log.square.col}`}
    </li>
  ));

  return (
    <div id="logs-container">
      <ol id="log">{logs.length > 0 ? logEntries : selectField}</ol>
    </div>
  );
}

export default Logs;
