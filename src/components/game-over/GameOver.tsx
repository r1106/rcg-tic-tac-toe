import { MouseEventHandler } from "react";

function GameOver({
  winnerName,
  onClick,
}: {
  winnerName: string;
  onClick: MouseEventHandler<HTMLButtonElement>;
}) {
  const winnerMessage: JSX.Element = <p>Winner is {winnerName}!</p>;
  const drawMessage: JSX.Element = <p>It's a Draw!</p>;

  return (
    <div id="game-over">
      <h2>Game Over</h2>
      {winnerName ? winnerMessage : drawMessage}
      <button onClick={onClick}>Rematch!</button>
    </div>
  );
}

export default GameOver;
