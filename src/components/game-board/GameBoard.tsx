import { TBoard } from "../../types/board.type";

function GameBoard({
  board,
  onClick,
}: {
  board: TBoard;
  onClick: (rowIndex: number, colIndex: number) => void;
}): JSX.Element {
  return (
    <ol id="game-board">
      {board.map((row, rowIndex) => {
        return (
          <ol key={rowIndex}>
            {row.map((playerSymbol, colIndex) => {
              return (
                <li key={colIndex}>
                  <button
                    onClick={() => onClick(rowIndex, colIndex)}
                    disabled={playerSymbol !== null}
                  >
                    {playerSymbol}
                  </button>
                </li>
              );
            })}
          </ol>
        );
      })}
    </ol>
  );
}

export default GameBoard;
