import { ReactElement, useState } from "react";

function Player({
  name,
  playerSymbol,
  isActive,
  onClick,
}: {
  name: string;
  playerSymbol: string;
  isActive: boolean;
  onClick: (symbol: string, name: string) => void;
}): JSX.Element {
  const [isEdit, setIsEdit] = useState(false);
  const [playerName, setPlayerName] = useState(name);

  function onEditClick(): void {
    setIsEdit((isEdit) => !isEdit);
    if (isEdit) {
      onClick(playerSymbol, playerName);
    }
  }

  function onSavePlayerName(name: string): void {
    setPlayerName(name);
  }

  const playerNameSpan: ReactElement<HTMLSpanElement> = (
    <span className="player-name">{playerName}</span>
  );
  const playerInput: ReactElement<HTMLInputElement> = (
    <input
      type="text"
      defaultValue={playerName}
      onChange={(event) => onSavePlayerName(event.target.value)}
    ></input>
  );

  return (
    <li className={`player ${isActive ? "active" : undefined}`}>
      {isEdit ? playerInput : playerNameSpan}
      <span className="player-symbol">{playerSymbol}</span>
      <button onClick={onEditClick}> {isEdit ? "Save" : "Edit"} </button>
    </li>
  );
}

export default Player;
