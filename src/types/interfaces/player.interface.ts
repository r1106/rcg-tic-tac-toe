import { SymbolsEnum } from "../enums/symbols.enum";

export interface IPlayer {
  symbol: SymbolsEnum;
  name: string;
}
