import { IPlayer } from "./player.interface";

export interface IPlayers {
  playerOne: IPlayer;
  playerTwo: IPlayer;
}

export default IPlayers;
