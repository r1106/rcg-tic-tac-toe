import { ISquare } from "./square.interface";

export interface ITurn {
  playerSymbol: string;
  square: ISquare;
}
