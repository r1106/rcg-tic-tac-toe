import { PlayernamesEnum } from "../enums/playernames.enum";
import { SymbolsEnum } from "../enums/symbols.enum";
import IPlayers from "../interfaces/players.interface";

export const PLAYERS: IPlayers = {
  playerOne: {
    symbol: SymbolsEnum.playerOne,
    name: PlayernamesEnum.playerOne,
  },
  playerTwo: {
    symbol: SymbolsEnum.playerTwo,
    name: PlayernamesEnum.playerTwo,
  },
};
