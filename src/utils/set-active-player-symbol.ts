import { SymbolsEnum } from "../types/enums/symbols.enum";
import { ITurn } from "../types/interfaces/turn.interface";

export function setActivePlayerSymbol(gameTurns: ITurn[]): string {
  return gameTurns[0]?.playerSymbol === SymbolsEnum.playerOne
    ? SymbolsEnum.playerTwo
    : SymbolsEnum.playerOne;
}
