import { TBoard } from "../types/board.type";
import { WINNING_COMBINATIONS } from "../types/consts/winning-combinations.const";
import IPlayers from "../types/interfaces/players.interface";

export function getWinnerName(
  gameBoard: TBoard,
  playerNames: IPlayers
): string {
  let winnerName: string = "";

  for (const combination of WINNING_COMBINATIONS) {
    const firstSymbol: string | null =
      gameBoard[combination[0].row][combination[0].col];
    const secondSymbol: string | null =
      gameBoard[combination[1].row][combination[1].col];
    const thirdSymbol: string | null =
      gameBoard[combination[2].row][combination[2].col];

    if (
      firstSymbol &&
      firstSymbol === secondSymbol &&
      firstSymbol === thirdSymbol
    ) {
      winnerName =
        playerNames.playerOne.symbol === firstSymbol
          ? playerNames.playerOne.name
          : playerNames.playerTwo.name;
    }
  }

  return winnerName;
}
