import { TBoard } from "../types/board.type";
import { BOARD } from "../types/consts/board.const";
import { ITurn } from "../types/interfaces/turn.interface";

export function setGameBoard(gameTurns: ITurn[]): TBoard {
  const turns: ITurn[] = [...gameTurns];
  const gameBoard: TBoard = [...BOARD.map((array) => [...array])];

  for (const turn of turns) {
    const { square, playerSymbol } = turn;
    const { row, col } = square;

    gameBoard[row][col] = playerSymbol;
  }

  return gameBoard;
}
