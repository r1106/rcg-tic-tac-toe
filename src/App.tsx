import { useState } from "react";
import "./App.scss";
import GameBoard from "./components/game-board/GameBoard";
import GameOver from "./components/game-over/GameOver";
import Header from "./components/header/Header";
import Logs from "./components/logs/Logs";
import Player from "./components/player/Player";
import { PLAYERS } from "./types/consts/players.const";
import { SymbolsEnum } from "./types/enums/symbols.enum";
import IPlayers from "./types/interfaces/players.interface";
import { ITurn } from "./types/interfaces/turn.interface";
import { getWinnerName } from "./utils/get-winner-name";
import { setActivePlayerSymbol } from "./utils/set-active-player-symbol";
import { setGameBoard } from "./utils/set-game-board";

function App(): JSX.Element {
  const [playerNames, setPlayerNames] = useState<IPlayers>({ ...PLAYERS });
  const [gameTurns, setGameTurns] = useState<ITurn[]>([]);
  const activePlayerSymbol = setActivePlayerSymbol(gameTurns);
  const gameBoard = setGameBoard(gameTurns);
  const gameWinnerName = getWinnerName(gameBoard, playerNames);
  const gameDraw = gameTurns.length === 9 && !gameWinnerName;

  function onGameBoardFieldClick(rowIndex: number, colIndex: number): void {
    setGameTurns((prevTurns) => {
      const currentPlayerSymbol: string = setActivePlayerSymbol(prevTurns);
      const updatedTurns: ITurn[] = [
        {
          square: { row: rowIndex, col: colIndex },
          playerSymbol: currentPlayerSymbol,
        },
        ...prevTurns,
      ];

      return updatedTurns;
    });
  }

  function onResetBoard(): void {
    setGameTurns([]);
  }

  function onPlayerNameSaved(symbol: string, name: string): void {
    if (symbol === SymbolsEnum.playerOne) {
      setPlayerNames((prevPlayers) => ({
        ...prevPlayers,
        playerOne: { ...prevPlayers.playerOne, name },
      }));
    } else if (symbol === SymbolsEnum.playerTwo) {
      setPlayerNames((prevPlayers) => ({
        ...prevPlayers,
        playerTwo: { ...prevPlayers.playerTwo, name },
      }));
    }
  }

  return (
    <>
      <section id="header-container">
        <Header></Header>
        <Logs turns={gameTurns}></Logs>
      </section>
      <section id="game-container">
        <ol id="players">
          <Player
            name={playerNames.playerOne.name}
            playerSymbol={SymbolsEnum.playerOne}
            isActive={activePlayerSymbol === SymbolsEnum.playerOne}
            onClick={onPlayerNameSaved}
          ></Player>
          <Player
            name={playerNames.playerTwo.name}
            playerSymbol={SymbolsEnum.playerTwo}
            isActive={activePlayerSymbol === SymbolsEnum.playerTwo}
            onClick={onPlayerNameSaved}
          ></Player>
        </ol>
        {(gameWinnerName || gameDraw) && (
          <GameOver
            winnerName={gameWinnerName}
            onClick={onResetBoard}
          ></GameOver>
        )}
        <GameBoard
          board={gameBoard}
          onClick={onGameBoardFieldClick}
        ></GameBoard>
      </section>
    </>
  );
}

export default App;
